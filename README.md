# `ppl_kit`

`ppl_kit` is functionality that I find helpful when using probabilistic
programming languages written in Python. So far this functionality targets 
`pyro`, but this need not remain the case. 

## `inference.metropolis`
Generic Metropolis-Hastings in Pyro. This modifies Eli Bingham's code
(nearly apocryphal -- stemming from a single git issue, never incorporated into pyro
at all) with some bug fixes,
performance improvements, and logging. 

## `inference.query`
I often find myself wanting to treat collections of probabilistic programs as 
a virtual relational database.
This allows you to easily do so. For example:

```
# Construct a posterior approximator -- subclass of pyro TracePosterior
mh = metropolis.SingleSiteMH(
    normal_model,
    samples=samples,
    lag=lag,
    burn=burn,
)
# Sample from the posterior
mh = mh.run(data)

# pass posterior model into a relational DB (sqlite by default)
# this db will have only one table, but in general you can pass
# many models and there will be one table per model
rmc = query.RelationalModelCollection((mh,), num_samples=250)

# passing predictive=True also includes columns related to the posterior predictive
rmc.create_db(None, predictive=True)

results = rmc.query_db(
    """
    select
        avg(loc), avg(scale), avg(data)
    from
        normal_model;
    """
)
logging.info(results)
```