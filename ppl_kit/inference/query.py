"""
Copyright David Rushing Dewhurst, 2022 - present. 

All rights reserved. 
"""

import pandas as pd
import pyro 
import sqlalchemy 
from sqlalchemy import text


REPLACE_STR = "__"


def trace_posterior_to_dataframe(trace_posterior, *args, num_samples=1, predictive=False, **kwargs):
    """
    Converts a `TracePosterior` to a pandas dataframe via sampling.

    :param trace_posterior: an object that subclasses `TracePosterior`
    :param args: any additional arguments to pass to the `trace_posterior`
    :param num_samples: number of samples. There will be `num_samples` rows in the dataframe.
    :param kwargs: any additional keyword arguments to pass to the `trace_posterior`
    """
    if not predictive:
        return _dataframe_from_traces([
            trace_posterior(*args, **kwargs) for _ in range(num_samples)
        ])
    else:
        return _dataframe_from_traces([
            trace_posterior.posterior_predictive(*args, **kwargs) for _ in range(num_samples)
        ])


def _filter_sites(tr, site,):
    """
    Filters sites in a trace to see if they are of sample type
    and have an `"is_observed"` key.
    """
    if (tr.nodes[site]["type"] == "sample") and ("is_observed" in tr.nodes[site].keys()):
        return True
    return False



def _dataframe_from_traces(traces):
    """
    Creates a pandas dataframe from an iterable of traces. 
    """
    bag = []
    for trace in traces:
        value = {}
        logl = 0.0
        for name in trace.nodes.keys():
            transformed_name = name.replace("/", REPLACE_STR)
            if _filter_sites(trace, name):
                try:
                    value[transformed_name] = trace.nodes[name]["value"].numpy().item()
                except Exception:
                    continue  # TODO: make this more clear...
                value[f"{transformed_name}_logprob"] = trace.nodes[name]["log_prob_sum"].numpy().item()
                if trace.nodes[name]["is_observed"]:
                    logl += trace.nodes[name]["log_prob_sum"].numpy().item()
        value["total_logprob"] = trace.log_prob_sum().numpy().item()
        value["total_loglikelihood"] = logl
        bag.append(value)
    return pd.DataFrame(bag)


class RelationalModelCollection:
    """
    Interprets a collection of trained models as a relational database.

    Each model is treated as a table in the database. The number of rows in 
    each table is equal to `num_samples`. 

    :param models: an iterable of objects that subclass `TracePosterior`
    :param db_address: the address of the database. E.g., "sqlite://".
    :param num_samples: number of samples. There will be `num_samples` rows in each
        created table.
    """

    def __init__(self, models, db_address='sqlite://', num_samples=1, names=None,):
        self._models = models
        self._num_samples = num_samples

        self._db_address = db_address
        self._engine = None

        if names is not None:
            assert len(names) == len(self._models)
        self._names = [m.name for m in self._models] if names is None else names

    def create_db(self, *args, predictive=False, **kwargs):
        """
        Creates a database. There will be a number of tables equal to the size of the
        model iterable, and each table will include `num_samples` rows. 
        """
        self._engine = sqlalchemy.create_engine(self._db_address)
        for (model, name) in zip(self._models, self._names):
            with self._engine.connect() as conn:
                conn.execute(text(f"drop table if exists {name};"))
            trace_posterior_to_dataframe(
                model,
                *args,
                num_samples=self._num_samples,
                predictive=predictive,
                **kwargs
            ).to_sql(name, con=self._engine)

    def query_db(self, query):
        """
        Issues a query to the database and returns results as a pandas dataframe. 

        :param query: a SQL string to be evaluated in the database
        """
        if self._engine is None:
            raise ValueError("Call .create_db() to populate the database.")
        with self._engine.connect() as conn:
            return pd.read_sql_query(
                query,
                conn,
            )
