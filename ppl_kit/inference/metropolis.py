"""
Copyright David Rushing Dewhurst, 2022 - present. 

All rights reserved. 

Fixes and updates to the generic MH implementation by Eli Bingham:
https://github.com/pyro-ppl/pyro/blob/76097a8e0d9463c151a8590ec286fde99e5597ba/examples/storyboard/mh.py
"""

import logging
import random

import pyro
import pyro.distributions as dist
import pyro.poutine as poutine
import torch


class MH(pyro.infer.TracePosterior):
    """
    Generic Metropolis Hastings. 

    :param model: a pyro model
    :param args: any position arguments to pass to the model
    :param guide: a variational posterior with signature
        `(trace, *args, **kwargs)`. At least one of `guide` or `proposal` must
        be defined.
    :param proposal: a proposal kernel with signature `(*args, **kwargs)`. At
        least one of `guide` or `proposal` must be defined.
    :param samples: number of samples to draw from the posterior. 
    :param lag: thinning rate. Every `lag`-th sample will be kept.
    :param burn: burn-in number. None of the first `burn` samples will be kept. 
    :param verbosity: print status updates every floor(1.0 / verbosity) iterations
    :param kwargs: any keyword arguments to pass to the model
    """
    def __init__(
        self,
        model,
        *args,
        guide=None,
        proposal=None, 
        samples=10,
        lag=1,
        burn=0,
        verbosity=1.0/500,
        **kwargs
    ):
        super().__init__()

        self.name = model.__name__

        self.samples = samples
        self.lag = lag
        self.burn = burn
        self.inverse_verbosity = int(1.0 / verbosity)

        self.model = model

        assert (guide is None or proposal is None) and \
            (guide is not None or proposal is not None), \
            "requires exactly one of guide or proposal, not both or none"
        if guide is not None:
            self.guide = lambda tr, *args, **kwargs: guide(*args, **kwargs)
        else:
            self.guide = proposal

    def __call__(self, *args, return_observed=False, **kwargs):
        """
        Overriding the default `TracePosterior` behavior to optionally also return the 
        observed sites

        :param return_observed: if `return_observed` is True, returns value and log
            probabilities associated with observed sites in addition to latent.
        """
        # To ensure deterministic sampling in the presence of multiple chains,
        # we get the index from ``idxs_by_chain`` instead of sampling from
        # the marginal directly.
        random_idx = self._categorical.sample().item()
        chain_idx, sample_idx = (
            random_idx % self.num_chains,
            random_idx // self.num_chains,
        )
        sample_idx = self._idx_by_chain[chain_idx][sample_idx]
        trace = self.exec_traces[sample_idx].copy()
        if not return_observed:
            for name in trace.observation_nodes:
                trace.remove_node(name)
        return trace

    def _traces(self, *args, **kwargs):
        """
        Creates the posterior distribution using Metropolis Hastings sampling.
        """
        # initialize traces with a draw from the prior
        old_model_trace = poutine.trace(self.model).get_trace(*args, **kwargs)
        traces = []
        t = 0
        i = 0

        while i < self.burn + self.lag * self.samples:
            i += 1
            # q(z' | z)
            new_guide_trace = poutine.trace(self.guide).get_trace(old_model_trace, *args, **kwargs)
            # p(x, z')
            new_model_trace = poutine.trace(
                poutine.replay(self.model, new_guide_trace)).get_trace(*args, **kwargs)
            # q(z | z')
            old_guide_trace = poutine.trace(
                    poutine.replay(self.guide, old_model_trace)).get_trace(new_model_trace,
                                                                  *args, **kwargs)
            # p(x, z') q(z' | z) / p(x, z) q(z | z')
            logr = new_model_trace.log_prob_sum() + new_guide_trace.log_prob_sum() - \
                old_model_trace.log_prob_sum() - old_guide_trace.log_prob_sum()
            rnd = pyro.sample("mh_step_{}".format(i),
                              dist.Uniform(torch.zeros(1), torch.ones(1)))

            if torch.log(rnd) < logr:
                # accept
                t += 1
                old_model_trace = new_model_trace
            if i > self.burn and i % self.lag == 0:
                yield (old_model_trace, old_model_trace.log_prob_sum())
            if i % self.inverse_verbosity == 0:
                logging.info(f"On iteration number {i}")

    def posterior_predictive(self, *args, **kwargs):
        the_trace = self.__call__(*args, **kwargs)
        return self._posterior_predictive(the_trace, *args, **kwargs)

    def posterior_predictive_at(self, ix, *args, **kwargs):
        trace = self.exec_traces[ix].copy()
        for name in trace.observation_nodes:
            trace.remove_node(name)
        return self._posterior_predictive(trace, *args, **kwargs)

    def _posterior_predictive(self, the_trace, *args, **kwargs):
        the_trace = pyro.poutine.trace(
            pyro.poutine.replay(self.model, the_trace)).get_trace(*args, **kwargs)
        the_trace.log_prob_sum()
        return the_trace
        

def _filter_sites(tr, site):
    """
    Filters sites. Returns true if the site is appropriate to use in a single-site
    proposal kernel.
    """
    if (tr.nodes[site]["type"] == "sample") and ("is_observed" in tr.nodes[site].keys()):
        if not tr.nodes[site]["is_observed"]:
            return True
    return False


def single_site_proposal(model):
    """
    Creates a single site proposal distribution by proposing from a random site
    from the model's prior.

    :param model: a pyro model
    """
    def _fn(tr, *args, **kwargs):
        choice_name = random.choice(
            [s for s in tr.nodes.keys() if _filter_sites(tr, s)]
        )
        return pyro.sample(choice_name,
                           tr.nodes[choice_name]["fn"],
                           *tr.nodes[choice_name]["args"],
                           **tr.nodes[choice_name]["kwargs"])
    return _fn


class SingleSiteMH(MH):
    """
    Propose from the prior to a single, randomly-chosen site at every call.
    """
    def __init__(self, model, *args, **kwargs):
        super().__init__(
            model,
            *args,
            guide=None,
            proposal=single_site_proposal(model),
            **kwargs
        )
