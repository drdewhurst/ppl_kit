
import logging

import pyro
import pyro.distributions as dist
import torch

from ppl_kit.inference import metropolis, query


def normal_model(data):
    loc = pyro.sample("loc", dist.Normal(0.0, 3.0))
    scale = pyro.sample("scale", dist.Gamma(2.0, 2.0))
    return pyro.sample("data", dist.Normal(loc, scale), obs=data)


def test_metropolis_single_site_basic():
    samples = 100
    lag = 100
    burn = 1000
    data = torch.tensor(-3.0)

    mh = metropolis.SingleSiteMH(
        normal_model,
        samples=samples,
        lag=lag,
        burn=burn,
    )
    mh = mh.run(data)
    
    df = query.trace_posterior_to_dataframe(mh, num_samples=250)

    logging.info(df)
    logging.info(f"Loc = ({df['loc'].mean()}, {df['loc'].std()})")
    logging.info(f"Scale = ({df['scale'].mean()}, {df['scale'].std()})")
    logging.info(mh.information_criterion())


def test_metropolis_single_site_query():
    samples = 100
    lag = 100
    burn = 1000
    data = torch.tensor(-3.0)

    mh = metropolis.SingleSiteMH(
        normal_model,
        samples=samples,
        lag=lag,
        burn=burn,
    )
    mh = mh.run(data)

    rmc = query.RelationalModelCollection((mh,), num_samples=250)
    rmc.create_db(None, predictive=True)

    results = rmc.query_db(
        """
        select
            avg(loc), avg(scale), avg(data)
        from
            normal_model;
        """
    )
    logging.info(results)


def open_universe_model(data, ix=0, value=0.0):
    while True:
        left_or_right = pyro.sample(f"lr_{ix}", dist.Bernoulli(0.6))
        ix += 1
        if left_or_right > 0:
            stop = pyro.sample(f"stop_right_{ix}", dist.Bernoulli(0.3))
            value += pyro.sample(f"value_right_{ix}", dist.Normal(1.0, 1.0))
            if stop > 0:
                return pyro.sample("total", dist.Normal(value, 1.0), obs=data,)
            else:
                return open_universe_model(data, ix=ix, value=value)
        else:
            stop = pyro.sample(f"stop_left_{ix}", dist.Bernoulli(0.3))
            value += pyro.sample(f"value_left_{ix}", dist.Normal(-1.0, 1.0))
            if stop > 0:
                return pyro.sample("total", dist.Normal(value, 1.0), obs=data,)
            else:
                return open_universe_model(data, ix=ix, value=value)


def test_metropolis_open_universe():
    data = torch.tensor(4.0)
    samples = 100
    lag = 100
    burn = 1000

    mh = metropolis.SingleSiteMH(
        open_universe_model,
        samples=samples,
        lag=lag,
        burn=burn,
    )
    mh = mh.run(data)

    rmc = query.RelationalModelCollection((mh,), num_samples=250, names=("zorgs",))
    rmc.create_db()
    results = rmc.query_db(
        """
        select * from zorgs;
        """
    )
    logging.info(results)
    